# Objective
This repo contains two simple ansible playbooks for communicating with legacy Cisco IOS Routers (Cisco ISR 2911) via ssh. Since we are communicating with older IOS devices, I used the network_cli connection method and had to specify ```ansible_network_os=ios``` variable.

  1. cisco_command.yml
  2. cisco_iosfacts.yml


Both playbooks leverage a common inventory (inventory) file and a variable file (creds.yml) for the device credentials

# cisco_command.yml
This playbook is run against all hosts and collects the results of CLI commands - show version, show interfaces, and show cdp neighbors and displays the three outputs.  Outputs are displayed individually using stdout_lines. 

To run the playbook, use command: `ansible-playbook -i inventory cisco_command.yml`


# cisco_iosfacts.yml
This playbook also runs against all hosts and collect ios_facts.

To run the playbook, use command: `ansible-playbook -i inventory cisco_playbook.yml`

# Notes
Useful debugging;
use``` -vvvv``` at the end of your playbook
```EXPORT debug_ANSIBLE=True``` (and ```EXPORT DEBUG_ANSIBLE=False```) if you are running ansible on a Mac
